﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleARCore.Examples.HelloAR;

public class ButtonChangeEffect : MonoBehaviour
{
    public HelloARController helloArController;
    public void ChangeEffecet(GameObject effect)
    {
        helloArController.AndyPlanePrefab = effect;
        helloArController.AndyPointPrefab = effect;
    }

}
