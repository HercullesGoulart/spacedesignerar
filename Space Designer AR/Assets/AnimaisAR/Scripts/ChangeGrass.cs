﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore.Examples.Common;

public class ChangeGrass : MonoBehaviour {
    public GameObject myGO;
    public DetectedPlaneGenerator changePlane;
    public void ChangeEffecet(GameObject effect)
    {
        changePlane.DetectedPlanePrefab = effect;
        Destroy(myGO.gameObject);
    }
    
}
